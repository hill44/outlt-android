package com.hill44.outlt.android.tasks;
import android.widget.ListView;

import com.hill44.outlt.android.adapters.ListViewAdapter;
import com.hill44.outlt.android.activities.MainActivity;
import com.hill44.outlt.android.beans.LoadMoreTaskBean;

public class LoadMoreTask extends AbstractLoaderTask {

    private int limit;

    public LoadMoreTask(LoadMoreTaskBean taskBean) {
        super(taskBean);
    }

    @Override
    protected void onPostExecute(Void result) {
        int position = super.getListview().getLastVisiblePosition();

        ListViewAdapter adapter = (ListViewAdapter)super.getListview().getAdapter();

        super.getListview().setAdapter(adapter);
        super.getListview().setSelectionFromTop(position, 0);
        super.getProgressDialog().dismiss();


    }

}