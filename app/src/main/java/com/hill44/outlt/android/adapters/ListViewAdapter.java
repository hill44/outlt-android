package com.hill44.outlt.android.adapters;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Date;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hill44.outlt.android.R;
import com.hill44.outlt.android.beans.EventBean;
import com.hill44.outlt.android.views.SingleItemView;

public class ListViewAdapter extends BaseAdapter {
	
	// Declare Variables
	Context mContext;
	LayoutInflater inflater;

	private List<EventBean> eventList;
	protected int count;

	public ListViewAdapter(Context context, List<EventBean> eventList) {
		mContext = context;
		this.eventList = eventList;
		inflater = LayoutInflater.from(mContext);
		this.eventList = new ArrayList<EventBean>();
		this.eventList.addAll(eventList);
	}

	public class ViewHolder {
		TextView num;
        TextView place;
        RelativeLayout setBG;
        TextView date;
	}

	@Override
	public int getCount() {
		return eventList.size();
	}

	@Override
	public EventBean getItem(int position) {
		return eventList.get(position);
	}

    public List<EventBean> getItems() {
        return this.eventList;
    }

    public void setItems(List<EventBean> events) {
        this.eventList = events;
    }

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View view, ViewGroup parent) {
		final ViewHolder holder;
		if (view == null) {
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.listview_item, null);
			// Locate the TextView in listview_item.xml
			holder.num = (TextView) view.findViewById(R.id.num);
            holder.place = (TextView) view.findViewById(R.id.place);
            holder.setBG = (RelativeLayout) view.findViewById(R.id.setBG);
            holder.date = (TextView)view.findViewById(R.id.date);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}


        String localDate = getDateISO8601(eventList.get(position).getStart_time());

        holder.num.setText(eventList.get(position).getName());
        holder.place.setText(eventList.get(position).getLocation());
        holder.date.setText(localDate);

		view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Send single item click data to SingleItemView Class
                Intent intent = new Intent(mContext, SingleItemView.class);

                // Pass all data
                intent.putExtra("eventID", (eventList.get(position).getEventID()));
                intent.putExtra("name", (eventList.get(position).getName()));
                intent.putExtra("startTime", (eventList.get(position).getStart_time()));
                intent.putExtra("endTime", (eventList.get(position).getEnd_time()));
                intent.putExtra("isDateOnly", (eventList.get(position).isIs_date_only()));
                intent.putExtra("location", (eventList.get(position).getLocation()));
                intent.putExtra("owner", (eventList.get(position).getOwner()));
                intent.putExtra("venue", (eventList.get(position).getVenue()));
                intent.putExtra("ticketURI", (eventList.get(position).getTicket_uri()));
                intent.putExtra("cover", (eventList.get(position).getCover()));
                intent.putExtra("drawableID", (eventList.get(position).getDrawableId()));

                // Start SingleItemView Class
                mContext.startActivity(intent);
            }
        });

        if (holder.setBG != null) {
            holder.setBG.setBackgroundResource(R.drawable.audi);
            // Load image before inserting here
            // TODO: Replace BitmapDrawable because it is deprecated and crashes program (API 15)
            //holder.setBG.setBackground(new BitmapDrawable(eventList.get(position).getCover()));
            //new ImageDownloaderTask(holder.setBG).execute(eventList.get(position).getCover());
        }

		return view;
	}

    private String getDateISO8601(String utcLongDateTime) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH);

        Date date = null;
        try {
            date = dateFormat.parse(utcLongDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date != null){
            return date.toString();
        }else {
            return "";
        }
    }
}