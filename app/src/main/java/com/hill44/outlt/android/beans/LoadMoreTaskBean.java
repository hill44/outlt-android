package com.hill44.outlt.android.beans;

import android.widget.ListView;

import com.hill44.outlt.android.activities.MainActivity;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by mindaugas on 15.5.28.
 */
public class LoadMoreTaskBean {
    private MainActivity context;
    private ListView listView;
    private int limit;
    private int skip;
    private List<ParseObject> events;

    public MainActivity getContext() {
        return context;
    }

    public void setContext(MainActivity context) {
        this.context = context;
    }

    public ListView getListView() {
        return listView;
    }

    public void setListView(ListView listView) {
        this.listView = listView;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public List<ParseObject> getEvents() {
        return events;
    }

    public void setEvents(List<ParseObject> events) {
        this.events = events;
    }
}
