package com.hill44.outlt.android.tasks;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import com.hill44.outlt.android.activities.MainActivity;
import com.hill44.outlt.android.adapters.ListViewAdapter;
import com.hill44.outlt.android.beans.EventBean;
import com.hill44.outlt.android.beans.LoadMoreTaskBean;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.parse.ParseException;


/**
 * Created by mindaugas on 15.5.21.
 */
public class AbstractLoaderTask extends AsyncTask<Void, Void, Void> {
    private MainActivity context;
    private List<EventBean> eventList = null;
    private int limit = 20;
    ListView listview;
    List<ParseObject> events;
    private int skip = 0;

    AbstractLoaderTask(MainActivity context,  ListView listview){
        this.context = context;
        this.listview = listview;
    }

    AbstractLoaderTask(MainActivity context,  ListView listview, int limit){
        this.context = context;
        this.listview = listview;
        this.limit = limit;
    }

    AbstractLoaderTask(LoadMoreTaskBean taskBean){
        this.context = taskBean.getContext();
        this.listview = taskBean.getListView();
        this.limit = taskBean.getLimit();
        this.skip = taskBean.getSkip();
        this.eventList = ((ListViewAdapter)this.listview.getAdapter()).getItems();
    }

    private ProgressDialog progressDialog;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog = new ProgressDialog(this.context);
        progressDialog.setTitle("set text for loading title");
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(false);
        progressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {

        if(eventList == null){
            eventList = new ArrayList<EventBean>();
        }

        try {

            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
                    "EventObject");

            query.orderByAscending("createdAt");
            query.setLimit(limit);
            query.setSkip(this.skip);

            events = query.find();

            for (ParseObject event : events) {
                EventBean eventAdd = new EventBean();

                eventAdd.setName((String) event.get("name"));
                eventAdd.setCover(new ImageDownloaderTask().downloadBitmap((String)event.get("cover")));
                eventAdd.setLocation((String) event.get("location"));
                eventAdd.setStart_time((String)event.get("start_time"));

                this.eventList.add(eventAdd);
            }
        } catch (ParseException e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    public MainActivity getContext() {
        return context;
    }

    public void setContext(MainActivity context) {
        this.context = context;
    }

    public List<EventBean> getEventList() {
        return eventList;
    }

    public void setEventList(List<EventBean> eventList) {
        this.eventList = eventList;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public ListView getListview() {
        return listview;
    }

    public void setListview(ListView listview) {
        this.listview = listview;
    }

    public List<ParseObject> getEvents() {
        return events;
    }

    public void setEvents(List<ParseObject> events) {
        this.events = events;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;

    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }
}
