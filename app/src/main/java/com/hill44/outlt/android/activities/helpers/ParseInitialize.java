package com.hill44.outlt.android.activities.helpers;

import com.hill44.outlt.android.activities.MainActivity;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;

/**
 * Created by mindaugas on 15.5.21.
 */
public class ParseInitialize {
     public ParseInitialize(MainActivity context) {
        // Add your initialization code here
        Parse.initialize(context, "siB0hGpZS3SGfkCUc7ZHJhjIHfCqUQxz1cufF98d",
                "Q1Apu5qdsKBDFXJmP0PEzQhrjs5tjM3FYQ59XwUI");

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        // If you would like all objects to be private by default, remove this
        // line.
        defaultACL.setPublicReadAccess(true);

        ParseACL.setDefaultACL(defaultACL, true);
    }
}
