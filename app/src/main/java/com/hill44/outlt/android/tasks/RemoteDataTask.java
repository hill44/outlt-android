package com.hill44.outlt.android.tasks;

import android.widget.ListView;

import com.hill44.outlt.android.adapters.ListViewAdapter;
import com.hill44.outlt.android.listiners.ParseScrollListiner;
import com.hill44.outlt.android.activities.MainActivity;

public class RemoteDataTask extends AbstractLoaderTask {

    public RemoteDataTask(MainActivity context,  ListView listview){
        super(context, listview);
    }

    @Override
    protected void onPostExecute(Void result) {
        ListViewAdapter adapter = new ListViewAdapter(super.getContext(), super.getEventList());

        super.getListview().setAdapter(adapter);

        super.getProgressDialog().dismiss();

        ParseScrollListiner parseScrollListiner = new ParseScrollListiner();
        parseScrollListiner.setContext(super.getContext());
        parseScrollListiner.setListView(super.getListview());
        super.getListview().setOnScrollListener(parseScrollListiner);
    }
}
