package com.hill44.outlt.android.views;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.hill44.outlt.android.R;

import java.io.PrintStream;

public class SingleItemView extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.singleitemview);

		// Add up action
		// TODO: Catch exception?
		ActionBar actionBar = getActionBar();
		getActionBar().setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setTitle(R.string.back);

		// Retrieve data from ListViewAdapter on click event
		Intent intent = getIntent();

		// Get the results
		String stringEventID = intent.getStringExtra("eventID");
		String stringName= intent.getStringExtra("name");
		String stringStartTime= intent.getStringExtra("startTime");
		String stringEndTime= intent.getStringExtra("endTime");
		String stringIsDateOnly= intent.getStringExtra("isDateOnly");
		String stringLocation= intent.getStringExtra("location");
		String stringOwner= intent.getStringExtra("owner");
		String stringVenue= intent.getStringExtra("venue");
		String stringTicketURI= intent.getStringExtra("ticketURI");
		String stringCover= intent.getStringExtra("cover");
		String stringDrawableID= intent.getStringExtra("drawableID");

		// Locate the TextView in singleitemview.xml
		ImageView imageView = (ImageView) findViewById(R.id.imageView);

		TextView textViewEventID = (TextView) findViewById(R.id.eventID);
		TextView textViewName = (TextView) findViewById(R.id.name);
		TextView textViewStartTime = (TextView) findViewById(R.id.startTime);
		TextView textViewEndTime = (TextView) findViewById(R.id.endTime);
		TextView textViewIsDateOnly = (TextView) findViewById(R.id.isDateOnly);
		TextView textViewLocation = (TextView) findViewById(R.id.location);
		TextView textViewOwner= (TextView) findViewById(R.id.owner);
		TextView textViewVenue = (TextView) findViewById(R.id.venue);
		TextView textViewTicketURI = (TextView) findViewById(R.id.ticketURI);
		TextView textViewCover = (TextView) findViewById(R.id.cover);
		TextView textViewDrawableID = (TextView) findViewById(R.id.drawableID);

		// Set the results into TextView
		imageView.setImageResource(R.drawable.audi);

		textViewEventID.setText(stringEventID);
		textViewName.setText(stringName);
		textViewStartTime.setText(stringStartTime);
		textViewEndTime.setText(stringEndTime);
		textViewIsDateOnly.setText(stringIsDateOnly);
		textViewLocation.setText(stringLocation);
		textViewOwner.setText(stringOwner);
		textViewVenue.setText(stringVenue);
		textViewTicketURI.setText(stringTicketURI);
		textViewCover.setText(stringCover);
		textViewDrawableID.setText(stringDrawableID);
	}

	// Navigate up to parent activity
	// TODO: Navigate up with a new back stack?
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

}