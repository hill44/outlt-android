package com.hill44.outlt.android.listiners;

import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.hill44.outlt.android.adapters.ListViewAdapter;
import com.hill44.outlt.android.beans.EventBean;
import com.hill44.outlt.android.beans.LoadMoreTaskBean;
import com.hill44.outlt.android.tasks.ImageDownloaderTask;
import com.hill44.outlt.android.tasks.LoadMoreTask;
import com.hill44.outlt.android.activities.MainActivity;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class ParseScrollListiner implements OnScrollListener {
	
	private ListView listview;

	private MainActivity context;
	
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		int threshold = 1;
		int count = listview.getCount();
		
		if (scrollState == SCROLL_STATE_IDLE) {
			if (listview.getLastVisiblePosition() >= count
					- threshold) {

                LoadMoreTaskBean loadMoreTaskBean = new LoadMoreTaskBean();
                loadMoreTaskBean.setContext(context);
                loadMoreTaskBean.setListView(listview);
                loadMoreTaskBean.setLimit(20);
                loadMoreTaskBean.setSkip(listview.getCount());

				LoadMoreTask loadMoreTask = new LoadMoreTask(loadMoreTaskBean);
				loadMoreTask.execute();
			}
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
        System.out.println("then this is started?");
	}


	public void setListView(ListView listview) {
		this.listview = listview;
	}

	public void setContext(MainActivity context) {
		this.context = context;
	}
}